#!/usr/bin/python

import sys, time, stat, errno, hashlib, zlib, re, os, glob, binascii
from collections import deque
from lib.fuse import FUSE, FuseOSError, Operations

FILE_READ_ONLY = stat.S_IFREG | 0444 #r--r--r--
DIR_READ_ONLY = stat.S_IFDIR | 0555 #r-xr-xr-x

HEADER = re.compile('^(?P<type>\w+) (?P<size>\d+)$')
TILDA_SELECTOR = re.compile('^(?P<name>.+?)~(?P<steps>\d+)$')

class GitFS(Operations):

   def __init__(self, root):
      self.root = root
      self.fh = 0

   def __getPathSpec(self, path):
      go = True
      bits = deque()
      while go:
         path = path.rstrip('/')
         prev = path
         path, name = os.path.split(path)
         if path == prev: #the end
            go = False
         else:
            bits.appendleft(name)
      return bits

   def __get(self, path):
      bits = self.__getPathSpec(path)
      node = self.root
      for bit in bits:
         node = node.expandOne(bit)
      return node

   def getattr(self, path, fh=None):
      try:
         node = self.__get(path)
      except OSError:
         raise FuseOSError(errno.ENOENT)
      return node.stat()

   def access(self, path, mode):
      try:
         node = self.__get(path)
      except OSError: #override errno
         raise FuseOSError(errno.EACCES)

   def readdir(self, path, fh):
      node = self.__get(path)
      return node.listdir()

   def open(self, path, flags):
      node = self.__get(path)
      fh = self.fh
      self.fh += 1
      return fh

   def read(self, path, length, offset, fh):
      #TODO check fh? 
      node = self.__get(path)
      if node.type != 'blob':
         raise FuseOSError(errno.ENOFILE)
      return node.content[offset:offset+length]

class Git:

   def __init__(self, roughGitPath):
      self.gitdir = self.__getGitDir(roughGitPath)

   def __getGitDir(self, inRepo):
      while '.git' not in os.listdir(inRepo):
         past = inRepo
         inRepo = os.path.abspath(os.path.join(inRepo, os.pardir))
         if past == inRepo: #tried to go past root
            raise AssertionError('no git repo found')
      repo = os.path.join(inRepo, '.git')
      assert os.path.isdir(repo)
      return repo

   def stat(self, path, size, mode):
      real = os.lstat(path)

      keys = ('st_atime', 'st_ctime', 'st_gid', 'st_mtime', 'st_nlink', 'st_uid')
      d = dict((key, getattr(real, key)) for key in keys)
      d['st_mode'] = mode
      d['st_size'] = size
      #TODO st_nlink? (remember linux vs osx incompatibility!)
      return d

   def getRefHash(self, name, subpath=['refs', 'heads']):
      path = [self.gitdir] + subpath + [name]
      path = os.path.join(*path)
      if os.path.exists(path):
         return self.slurp(path).strip() #TODO make sure this is a real hash?
      raise OSError(errno.ENOENT)

   def getFullObjectHash(self, hash):
      prefix, suffix = hash[0:2], hash[2:]
      match = os.path.join(self.gitdir, 'objects', prefix, suffix + '*')
      paths = glob.glob(match)
      if len(paths) == 1:
         path = paths.pop()
         if os.path.exists(path):
            path = path.rstrip(os.path.sep)
            path, suffix = os.path.split(path)
            path = path.rstrip(os.path.sep)
            path, prefix = os.path.split(path)
            path = prefix + suffix
            return path
      return None

   def getObjectPath(self, hash):
      prefix, suffix = hash[0:2], hash[2:]
      path = os.path.join(self.gitdir, 'objects', prefix, suffix)
      assert os.path.exists(path)
      return path

   def slurp(self, path):
      return open(path, 'r').read()

   def readObject(self, hash, compressed):
      raw = zlib.decompress(compressed)
      assert hashlib.sha1(raw).hexdigest() == hash
      i = raw.index('\0')
      assert i > 0
      header = raw[0:i]
      content = raw[i+1:]
      m = HEADER.match(header)
      assert m
      type = m.group('type')
      size = int(m.group('size'))
      assert len(content) == size
      return (type, content)

   def makeObject(self, type, content):
      assert type in ('blob', 'tree')
      raw = '%s %d\0%s' % (type, len(content), content)
      hash = hashlib.sha1(raw).hexdigest()
      compressed = zlib.compress(raw)
      return (hash, compressed)

   def getCommit(self, hash):
      compressed = self.slurp(self.getObjectPath(hash))
      type, content = self.readObject(hash, compressed)
      assert type == 'commit'
      lines = content.split('\n')

      commit = { 'parents':[] }

      def cmd(name, line):
         assert line.startswith(name)
         line = line[len(name) + 1:].strip()
         commit[name] = line

      cmd('tree',      lines[0])
      i = 1
      while lines[i].startswith('parent'):
         parent = lines[i][len('parent') + 1:].strip()
         i += 1
         commit['parents'].append(parent)

      cmd('author',    lines[i])
      cmd('committer', lines[i + 1])

      commit['message'] = lines[4].strip()
      commit['description'] = '\n'.join(l.strip() for l in lines[5:]).strip()

      return commit


#backed by a physical dir (for stat info) but with a map of provided children
class GitDirTree:

   def __init__(self, git, path, children):
      self.git = git
      self.path = os.path.join(git, path)
      assert os.path.isdir(self.path)
      self.children = children

   def stat(self):
      return self.git.stat(self.path, 0, DIR_READ_ONLY)

   def listdir(self):
      return self.children.keys()

   def expandOne(self, name):
      try:
         return self.children[name]
      except KeyError:
         raise OSError(errno.ENOENT)


#passthrough tree, but allows some magic redirection, so eg. HEAD^ or hash~3
#can be looked up by this tree and delegated to the real hash tree transparent
#to the caller
class GitCommitTree:

   def __init__(self, git, commitSpecPath):
      self.git = git
      self.path = os.path.join(git.gitdir, *commitSpecPath)
      assert os.path.isdir(self.path)
      self.specPath = commitSpecPath
      #self.tree = GitObjectTree(git, '/', DIR_READ_ONLY, commit['tree'])

   def stat(self):
      return self.git.stat(self.path, 0, DIR_READ_ONLY)

   def listdir(self):
      return os.listdir(self.path)

   def expandOne(self, name):
      subtract = 0
      m = TILDA_SELECTOR.match(name)
      if m:
         name = m.group('name')
         try:
            subtract = int(m.group('steps'))
         except ValueError, e:
            raise OSError(errno.ENOENT)

      else:
         while name.endswith('^'): #get parent ... XXX how does this work on merge commits?
            subtract += 1
            name = name[0:-1]

      commit = self.git.getCommit(self.git.getRefHash(name, self.specPath))
      while commit and subtract > 0:
         if 'parents' not in commit:
            raise OSError(errno.ENOENT)
         subtract -= 1

         parents = commit['parents']
         if not parents:
            raise OSError(errno.ENOENT)
         elif len(parents) > 1:
            print 'ambiguous parent!'
            raise OSError(errno.ENOENT) #TODO handle better?

         commit = self.git.getCommit(parents[0])

      if not commit or 'tree' not in commit:
         raise OSError(errno.ENOENT)
      return self.__get(commit)

   def __get(self, commit):
      #TODO cache!
      return GitObjectTree(self.git, '/', DIR_READ_ONLY, commit['tree'])
      

#backed by a git tree (or blob) object under the .git/objects dir
class GitObjectTree:

   def __init__(self, git, name, mode, sha):
      f = git.slurp(git.getObjectPath(sha))
      self.type, raw = git.readObject(sha, f)
      assert self.type in ('blob', 'tree')

      self.git = git
      self.name = name
      self.mode = mode
      self.sha = sha

      if self.type == 'blob':
         self.content = raw
      elif self.type == 'tree':
         self.entries = self.__parseTree(raw)

   def stat(self):
      #TODO use mode (possibly -w)
      #TODO handle other mode types too, eg. symlinks (+ submodule bs?)
      if self.type == 'tree':
         mode = DIR_READ_ONLY
         size = 0
      else:
         mode = FILE_READ_ONLY
         size = len(self.content) 

      hash = self.sha
      rootpath = [ self.git.gitdir, 'objects', hash[:2], hash[2:] ]
      rootpath = os.path.abspath(os.path.join(*rootpath))
      return self.git.stat(rootpath, size, mode)

   def listdir(self):
      if self.type != 'tree':
         raise OSError(errno.ENOTDIR)
      return self.entries.keys()

   def __expandAll(self):
      for name, entry in self.entries.items():
         self.__expandOne(name)

   def expandOne(self, name):
      if self.type != 'tree':
         raise OSError(errno.ENOTDIR)
      if name not in self.entries:
         raise OSError(errno.ENOENT)

      entry = self.entries[name]
      if 'node' in entry:
         return entry['node']

      node = GitObjectTree(self.git, **entry)
      entry['node'] = node
      return node

   def __parseTree(self, content):
      entries = {}

      while content:
         i = content.find(' ')
         assert i
         mode = int(content[:i])

         content = content[i+1:]
         i = content.find('\0')
         assert i
         name = content[:i]

         content = content[i+1:]
         sha = binascii.hexlify(content[:20])
         content = content[20:]

         entries[name] = {'mode':mode, 'sha':sha, 'name':name}

      return entries


def debug(git):
   git = Git(git)

   #hash = git.getRefHash('master')
   #commit = git.getCommit(hash)
   #master = git.makeCommitTree(commit)
   heads = GitCommitTree(git, ['refs', 'heads'])

   root = {'heads':heads}
   return GitDirTree(git, '/', root)

   #hash = git.getFullObjectHash('745bc') #commit
   #hash = git.getFullObjectHash('b5') #tree
   #hash = git.getFullObjectHash('d8') #old tree
   #hash = git.getFullObjectHash('ca44') #io tree
   hash = git.getFullObjectHash('b98') #w/ stat
   tree = GitObjectTree(git, '/', DIR_READ_ONLY, hash)
   return tree


if __name__ == '__main__':
   if len(sys.argv) != 3:
      print 'usage: %s <path-to-repo> <mountpoint>' % sys.argv[0]
      sys.exit(1)
   git, mount = sys.argv[1:]

   git = debug(git)
   FUSE(GitFS(git), mount, foreground=True)

